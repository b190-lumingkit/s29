// SECTION - Comparison Query Operators

// $gt/$gte operator
/* 
    - find documents that have field number values greater than or greater than/equal  to a specified number
    SYNTAXT:
        db.collectionName.find({ field : { $gt : value } })
        db.collectionName.find({ field : { $gte : value } })
*/
// greater than
db.users.find({ age: { $gt: 50 }});
// greater than or equal to
db.users.find({ age: { $gte: 21 }});

// $lt/$lte operator
/* 
    - find documents that have field number values less than or less than/equal to a specified number
    SYNTAXT:
        db.collectionName.find({ field : { $lt : value } })
*/
// less than
db.users.find({ age: { $lt: 50 }});
// less than or equal to
db.users.find({ age: { $lte: 50 }});

// $ne operator
/* 
    - find documents that have field values not equal to the specified value
    SYNTAX
        db.collectionName.find({ field : { $ne : value } })
*/
db.users.find({ age: { $ne: 82 }});

// $in operator
/* 
    - find documents with specific math criteria on one field using different values
    SYNTAX:
        db.collectionName.find({ field: { $in: value } });
*/
db.users.find({ lastName: { $in: ["Hawking", "Smith"] } });
db.users.find({ courses: { $in: ["React"] } });

// SECTION - Logical Query Operators
// $or operator
/* 
    - find documents matching a single criteria from multiple provided search query
    SYNTAX:
        db.collectionName.find({ $or: [ {fieldA: valueA}, {fieldB: valueB} ] });
*/
db.users.find({ $or: [{ firstName: "Neil" }, { age: 21 }] });
// combine $or and $gt operator
db.users.find({ $or: [{ firstName: "Neil" }, { age: { $gt: 21 } }] });

// $and operator
/* 
    - find documents matching multiple criteria in a single field
    SYNTAX:
        db.collectionName.find({ $and: [ {fieldA: valueA}, {fieldB: valueB} ] });
*/
db.users.find({ $and: [ {lastName: "Hawking"}, {age: {$ne: 82}} ] });
db.users.find({ $and: [ {age: {$ne: 76}}, {age: {$lte: 82}} ] });

// SECTION - Field Projection
/* 
    - retrieving documents are common operations that we do and by default MongoDB queries return the whole documents as a response
    - when dealing with complex data structure there might be instances while fields are not useful for the query that we are trying to accomplish
*/

// INCLUSION
/* 
    - included specific fields only when retrieving documents
    - the value provided is 1 to denote that the field is being included
    SYNTAX:
        db.collectionName.find( {criteria}, {field: 1});
*/
db.users.find(
    { firstName: "Jane" },
    {
        firstName: 1,
        lastName: 1,
        contact: 1
    }
);

// EXCLUSION
/* 
    - exclude specific fields only when retrieving documents
    - the value provided is 0 to denote that the field is being excluded
    SYNTAX:
        db.collectionName.find( {critera}, {field: 0} );
*/
db.users.find(
    { firstName: "Stephen" },
    {
        contact: 0,
        department: 0
    }
);

// mini-activity, suppressing of ID field
/* 
    - can only apply both exclusion and incluson in the ID field
*/
db.users.find(
    { firstName: "Neil" },
    {
        _id: 0,
        firstName: 1,
        lastName: 1,
        contact: 1
    }
);

db.users.find(
    {firstName: "Jane"},
    {
        _id: 0,
        firstName: 1,
        lastName: 1,
        contact: {phone: 1}
    }
);

db.users.find(
    {firstName: "Jane"},
    {
        _id: 0,
        firstName: 1,
        lastName: 1,
        contact: {email: 1}
    }
);

db.users.find(
    {firstName: "Jane"},
    {
        "contact.phone" : 0
    }
);

// Project Specific Array Elements in the Returned Array

// $slice operator
/* 
    SYNTAX:
        db.collectionName.find(
            {
                arrayName: {startIndex: "value"}
            },
            {
                arrayName: { $slice: numberOfElements }
            }
        )
*/
db.users.find(
    {
        nameArr: {nameA: "Juan"},
    },
    {
        nameArr:{$slice: 1}
    }
);

// SECTION - Evaluation Query Operators
// $regex operator
/* 
    - Allows us to find documents that match a specific string pattern using regular expressions
    SYNTAX:
        db.collectionName.find({ field: {$regex: "pattern", $options: "$optionsValue"} })
*/
// Case-sensitive query
db.users.find({ firstName: { $regex: "n" } })
// regex with options
db.users.find({ firstName: { $regex: "n", $options: "$i" } })
// works the same as
db.users.find({ firstName: { $regex: "n||N" } })