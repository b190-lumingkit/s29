// find users with letter s iin their firstname or t in their last name
db.users.find(
    {$or: [ {firstName: {$regex: "s||S"}}, {lastName: {$regex: "t||T"}} ]},
    {
        _id: 0,
        firstName: 1,
        lastName: 1
    }
);

// find users from HR department and their age is greater than or equal to 70
db.users.find(
    {$and: [ {department: "HR"}, { age: {$gte: 70 }} ]}
);

// find users with letter e in their first name and has an age of less than or equal to 30
db.users.find(
    {$and: [ { firstName: {$regex: "e||E"} }, { age: {$lte: 30} } ]}
);